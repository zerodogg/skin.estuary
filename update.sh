#!/bin/bash
cat << EOF
To update this version with the latest upstream stable release, check out the
upstream-stable branch, run the update.sh script that is in that branch, and
then merge that branch into the no-ratings-stable branch.
EOF
